/*jshint node: true */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var forbiddenAddressSchema = new Schema({
    url: String,
    selected: Boolean
});

module.exports = mongoose.model("ForbiddenAddress", forbiddenAddressSchema);