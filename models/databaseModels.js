/*jshint node: true */
var mongoose = require('mongoose');
var models = {
    User: require('./user'),
    ForbiddenAddress: require('./forbiddenAddress'),
    ActivityStats: require('./activityStats'),
    initializeData: function(modelName, JSON_models_array) {
        models[modelName].find(function(err, objects) {
          if (err) return console.error(err);
            if(objects.length === 0) {
                helpInit(modelName, JSON_models_array);
            } else {
                console.log('test data already exists in db, skipping initialization');
            }
        });

    }
};
//function encodeKey(key) {
//    return key.replace("\\", "\\\\").replace("\$", "\\u0024").replace(".", "\\u002e");
//}
function helpInit (modelName, JSON_models_array) {
    //console.log(modelName + ' ' + JSON.stringify(JSON_models_array));
     //console.log(models[modelName] + ' && ' + JSON_models_array.length);
      if(models[modelName]  && JSON_models_array.length >= 0) {
        JSON_models_array.forEach(function(val, index) {
            var _object =  models[modelName];
            _object = new _object(val);
            _object.save(function(err, thor) {
                  if (err) return console.error(err);
                    console.log(JSON.stringify(thor));
                 });
        });
    } else {
        throw new Error("error while initializing start-up data. \n model name: " + modelName + " \n " + JSON.stringify(JSON_models_array));
    }   
}
module.exports = models;