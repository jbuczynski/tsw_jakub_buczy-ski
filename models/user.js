/*jshint node: true */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var userSchema = new Schema({
    //id: String,
    name: String,
    username: { 
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
   userType: {type: String, enum: ['admin', 'user']}
});

module.exports = mongoose.model("User", userSchema);