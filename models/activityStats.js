/*jshint node: true */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var activitystats = new Schema({
    id: { 
        type: String,
        required: true,
        unique: true
    },
    websites: {}
});

module.exports = mongoose.model("ActivityStats", activitystats);