/* jshint node: true */
var crypto = require('crypto');
module.exports = function(passwordToHash, callbackFunction) {
    crypto.pbkdf2(passwordToHash, 'salt', 4096, 512, 'sha256', callbackFunction ? function(err, key) {
      if (err) {
        throw err;
      }
      callbackFunction(key.toString('hex'));
    } : function(err, key) {
      if (err) {
        throw err;
      }
      console.log(key.toString('hex') + ' \n ');
    });   
};
