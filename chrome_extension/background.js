/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false, $: false, chrome: false*/
"use strict";

var socket;

var SERVER_HOST = 'http://localhost';
var SERVER_PORT = 3000;

var siteInfoPayload = { timeStart: null, timeEnd: null, url: ''};

var onLogin = function(credentials) {
    if (!socket || !socket.connected) {
            socket = io(SERVER_HOST + ":" + SERVER_PORT, {forceNew: true});
    }
            socket.isAuthenticated = credentials.isAuthenticated;

            socket.user = credentials.user;
            socket.userType = credentials.user.userType;
            socket.on('connect', function () {  
            //socket.emit('toServer', 'hello server');
            socket.on('socketOnTimeOverrun', function(text) {
                alert(text);
            });
            socket.on('helloClient', function(data) {
                    console.log(data);
            });
            socket.on('onGetUserDetails', function(data) {
                console.log('onGetUserDetails');
                if(data && socket.isAuthenticated && socket.userType === 'admin') {
                    var viewTabUrl = chrome.extension.getURL('/html/admin.html');
                    var views = chrome.extension.getViews();
                    for (var i = 0; i < views.length; i++) {
                        var view = views[i];
                        if (view.location.href == viewTabUrl) {
                            console.log('wysylam toClient');
                            view.setUserDetails(data);
                          break;
                        }
                   }
                }
            });
            socket.on('toClient', function(data) {
                console.log(data + ' ' + socket.isAuthenticated);
                if(data && socket.isAuthenticated && socket.userType === 'admin') {
                    var viewTabUrl = chrome.extension.getURL('/html/admin.html');
                    var views = chrome.extension.getViews();
                    console.log('przed wysylam toClient');
                    for (var i = 0; i < views.length; i++) {
                        var view = views[i];
                        if (view.location.href == viewTabUrl) {
                            console.log('wysylam toClient');
                          view.serverDataPayload(data);
                        }
                  }
                } else if(data && socket.isAuthenticated && socket.userType === 'user') {
                    var viewTabUrl = chrome.extension.getURL('/html/popup.html');
                    var views = chrome.extension.getViews();
                    console.log('przed wysylam toClient');
                    for (var i = 0; i < views.length; i++) {
                        var view = views[i];                        
                        if (view.location.href == viewTabUrl) {
                            view.serverDatUserDataPayload(data);
                            break;
                        }
                  }
                } else {
                    chrome.tabs.getSelected(null, function(tab) {
                       chrome.tabs.update(tab.tabId, {url: chrome.extension.getURL('html/unauthorized.html')}, function () {});
                    });
                    
                }
            });
            socket.on('disconnect', function(data) {
                    if(data && socket.isAuthenticated && socket.userType === 'admin') {
                    var viewTabUrl = chrome.extension.getURL('/html/admin.html');
                    var views = chrome.extension.getViews();
                    console.log('przed wysylam toClient');
                    for (var i = 0; i < views.length; i++) {
                        var view = views[i];
                        if (view.location.href == viewTabUrl) {
                            view.location.href = chrome.extension.getURL('html/unauthorized.html');
                        }
                  }
                    console.log('zegnaj :(((');
                    }
            socket.isAuthenticated = false;

            socket.user = null;
            socket.userType = null;
            });
        });
      if(socket.user.userType === 'admin') {
            goToPage('html/admin.html');
        }
};
var goToMainPage = function()  {
    goToPage('html/index.html');
};

var goToAdminPage = function()  {
    goToPage('html/admin.html');
};

function goToPage(url) {
    var viewTabUrl = chrome.extension.getURL(url);
    chrome.tabs.create({url: viewTabUrl});    
}

function addUser(user) {
   if(!socket || !socket.isAuthenticated || !user) {
        console.error('error while adding user');
    } else {
        console.log('dodaje usera');
        socket.emit('addUser', user);
    }
}

function updateUser(user) {    
   if(!socket || !socket.isAuthenticated || !user) {
        console.error('error while editing user');
    } else {
        socket.emit('updateUser', user);
    }
}

function addForbiddenAddress(address) {
   if(!socket || !socket.isAuthenticated || !address) {
        console.error('error while adding address');
    } else {
        console.log('addForbiddenAddress');
        socket.emit('addForbiddenAddress', address);
    }
}

function removeForbiddenAddress(address) {
   if(!socket || !socket.isAuthenticated || !address) {
        console.error('error while reonUserDetailsRequestmoving address');
    } else {
        console.log('addForbiddenAddress');
        socket.emit('removeForbiddenAddress', address);
    }
}

function sendUserDataPayload(data) {
     if(!socket || !socket.isAuthenticated || !data) {
        console.error('error while sending user info');
    } else {
       console.log('userInfoPayload');
       data['userId'] = socket.user._id;
       data['username'] = socket.user.username;
       socket.emit('userInfoPayload', data);
    }    
}

function getUserDetails(userId) {
     if(!socket || !socket.isAuthenticated || !userId) {
        console.error('error while getting user details');
    } else {
       console.log('getUserDetails');
       socket.emit('onUserDetailsRequest', userId);
    }    
}

function checkIfLogged() {
     if(socket && socket.isAuthenticated ) {
        var popupView;
        var viewTabUrl = chrome.extension.getURL('/html/popup.html');
        var views = chrome.extension.getViews();
        console.log('przed wysylam toClient');
        for (var i = 0; i < views.length; i++) {
            var view = views[i];                        
            if (view.location.href == viewTabUrl) {
                popupView = view;
              break;
            }
      }
        if(socket.user.userType === 'admin' && popupView) {
            //update popup
            popupView.handleLogin({username: socket.user.username, userType: socket.user.userType});
        } else if(popupView){
             popupView.handleLogin({username: socket.user.username, userType: socket.user.userType});
        } else {
            console.error('cos nie tak');
        }
    } 
}

function logout(userId) {
     if(socket || socket.isAuthenticated ) {
       socket.emit('logout', userId);
    }    
}

function getSocketId() {
     if(socket) {
       return socket.id;
    } else {
        
    }
}

var updateTiming = (function updateTiming(initConfig) {
    var siteRegexp = /^(\w+:\/\/[^\/]+).*$/;
    var currentSite = null;
    var currentTabId = null;
    var startTime = null;
    var updateTimingInterval = 1000 * 30;  // 0.5 minute.
    
    function sendToServer(site, seconds) {
        
        sendUserDataPayload({timeSpent: seconds, url: site});
    }
    

      chrome.tabs.onSelectionChanged.addListener(
      function(tabId, selectionInfo) {
        console.log("Tab changed");
        currentTabId = tabId;
        runTiming();
      });


        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
         var viewTabUrl = chrome.extension.getURL('html/admin.html');
            if (tabId == currentTabId) {
              console.log("Tab updated");
              runTiming();
            }
            
          if(changeInfo.status == "complete") { 
            if(tab.url.trim() == viewTabUrl.trim()) {
                console.log('userTypreee' + socket.userType);
                if(!socket || !socket.isAuthenticated || socket.userType === 'user') {
                    chrome.tabs.update(tab.tabId, {url: chrome.extension.getURL('html/unauthorized.html')}, function () {});
                } else {
                    socket.emit('toServer', 'hello server');
                }
            }
          }
        });

      chrome.windows.onFocusChanged.addListener(
      function(windowId) {
        console.log("Detected window focus changed.");
        chrome.tabs.getSelected(windowId,
        function(tab) {
          console.log("Window/Tab changed");
          currentTabId = tab.id;
          runTiming();
        });
      }); 
    
    function runTiming() {
          if (!currentTabId) {
            return;
          }

        chrome.tabs.get(currentTabId, function(tab) {
        chrome.windows.get(tab.windowId, function(window) {
          if (!window.focused) {
            return;
          }
          var match = tab.url.match(siteRegexp);
          var site = null;
          if (match) {
            site = match[1];
          } else {
            console.log("wrong url.");
          }

          if (!currentSite) {
            currentSite = site;
            startTime = new Date();
            return;
          }

          var now = new Date();
          var delta = now.getTime() - startTime.getTime();
          sendToServer(currentSite, delta/1000);
          currentSite = site;
          startTime = now;
        });
      });
    }
    return {
        run: runTiming
    };
})();
