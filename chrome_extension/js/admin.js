/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false, $: false, chrome: false, angular: false*/
"use strict";

var adminPageApp = angular.module('adminPage', ['ngRoute', 'listview']);

adminPageApp.controller('usersController', ['$scope', function($scope) {

    $scope.collectionOfUsers = [];
    $scope.forbiddenAddresses = [];
    $scope.showModal = false;
    $scope.currentUser = {};
    $scope.currentAddress ='';
    $scope.userDetails = {};
    $scope.modalBackend =  {
        'title': '',
        'id': '',
        'username': '',
        'password': '',
        'userType': ''
    };
    $scope.toggleModal = function(){
        $scope.showModal = !$scope.showModal;
    };
    $scope.addUser = function addUser() {
        $('modal').attr('title','Dodaj nowego użytkownika');
        $scope.modalBackend['title'] = 'Dodaj nowego użytkownika';
        $scope.modalBackend['username'] = '';
        $scope.modalBackend['password'] = '';
        $scope.modalBackend['userType'] = '';
        $scope.toggleModal();

    }
    $scope.editUser = function editUser(user) {
         $('modal').attr('title','Edytuj użytkownika');
        $scope.modalBackend['title'] = 'Edytuj użytkownika';
        $scope.modalBackend['_id'] =  user._id;
        $scope.modalBackend['username'] = user.username;
        $scope.modalBackend['password'] = 'bedzie password';
        $scope.modalBackend['userType'] = user.userType;
        $scope.toggleModal();
    };
    
    $scope.sendForm = function sendForm() {
        var user = $.extend({}, $scope.modalBackend);
        delete user['title'];
        if($scope.modalBackend['title'] === 'Dodaj nowego użytkownika') {
            chrome.extension.getBackgroundPage().addUser(user);
            $scope.collectionOfUsers.push({
                username: $scope.modalBackend['username'],
                password: $scope.modalBackend['password'],
                userType: $scope.modalBackend['userType']
            })
        } else if ( $scope.modalBackend['title'] === 'Edytuj użytkownika'){
            chrome.extension.getBackgroundPage().updateUser(user);
             $.grep($scope.collectionOfUsers, function(e){ if( e._id === user._id) {
                e.username = $scope.modalBackend['username'],
                e.password = $scope.modalBackend['password'],
                e.userType = $scope.modalBackend['userType']
             }});
            
        }
        $scope.toggleModal();
    };
    
     $scope.onUserSelected = function onUserSelected(user) {
         chrome.extension.getBackgroundPage().getUserDetails(user._id);
     };
     $scope.addAddress = function addAdress() {
         
         if($scope.currentAddress) {
             var validUrl = getHostName($scope.currentAddress);
             var fixedUrl = handleNonUrl($scope.currentAddress);
             if( validUrl ) {
                chrome.extension.getBackgroundPage().addForbiddenAddress({url: $scope.currentAddress, selected: true});
                $scope.forbiddenAddresses.push({url: $scope.currentAddress, selected: true});
             } else if(fixedUrl) {
                $scope.currentAddress = fixedUrl;
                chrome.extension.getBackgroundPage().addForbiddenAddress({url: fixedUrl, selected: true});
                $scope.forbiddenAddresses.push({url: fixedUrl, selected: true});
             } else {
                 console.error('invalid url');
             }
         }
     };
}]);

adminPageApp.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value === true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });

window.onload = function() {  

};
var serverDataPayload = function (data) {
     var scope = angular.element($(".usersPanel")).scope();
        scope.$apply(function(){
            scope.collectionOfUsers = data.users;
            scope.forbiddenAddresses = data.forbiddenWebsites;
        });     
};

var setUserDetails = function (data) {
     var scope = angular.element($(".usersPanel")).scope();
        scope.$apply(function(){
            if(!data || typeof data == 'null') {          
                scope.userDetails = {};
            } else {
                scope.userDetails = data;
            }
        });     
};

function getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 &&
        typeof match[2] === 'string' && match[2].length > 0) {
    return match[2];
    }
    else {
        return null;
    }
}

function handleNonUrl(url) {
     var match = url.match(/(www[0-9]?\.)?(.[^\/:]+)/i);
     if (match != null && match.length > 2 &&
        typeof match[2] === 'string' && match[2].length > 0) {
            if(typeof match[1] === 'string' && match[1].length > 0) {
                console.log('converting invalid url to http protocol url');
                return 'http://' + match[1] + match[2];
            } else {
                console.log('converting invalid url to http protocol url');
                return 'http://' + match[2];
            }
    }
    else {
        return null;
    }
}

