/*jshint node: true */

var app = require("express")();
var httpServer = require("http").Server(app);
var io = require("socket.io")(httpServer);
var static = require('serve-static');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');
var mongoose = require('mongoose');
var crypto = require('./myCrypto');
var uuid = require('node-uuid');

//GLOBALS
var BREAK_TIME = 60; //in minutes
var socketPersistInterval = 50;

var collectionOfUsers = [];
var forbiddenAddresses = [];
var activityStatsCollection = [];
var informed = false;

// Passport.js
var passport = require('passport');
var passportLocal = require('passport-local');
var passportHttp = require('passport-http');

var port = process.env.PORT || 3000;
var secret = process.env.APP_SECRET || '$sekretny $sekret';

app.use('/js/bootstrap/bootstrap.min.js', static(__dirname + '/bower_components/bootstrap/dist/js/bootstrap.min.js'));
app.use('/style/angular-ui-list-view.css', static(__dirname + '/bower_components/angular-ui-list-view/dist/css/angular-ui-list-view.css'));
app.use('/style/bootstrap.css', static(__dirname + '/bower_components/bootstrap/dist/css/bootstrap.css'));
app.use('/style/bootstrap.css.map', static(__dirname + '/bower_components/bootstrap/dist/css/bootstrap.css.map'));
app.use('/js/jquery.min.js', static(__dirname + '/bower_components/jquery/dist/jquery.min.js'));
app.use('/js/jquery.min.map', static(__dirname + '/bower_components/jquery/dist/jquery.min.map'));
//app.use('/js/angular/angular.js', static(__dirname + '/bower_components/angular/angular.js'));
app.use('/js/angular/angular.min.js', static(__dirname + '/bower_components/angular/angular.min.js'));
app.use('/js/angular/angular.min.js.map', static(__dirname + '/bower_components/angular/angular.min.js.map'));
app.use('/js/angular/angular-route.min.js.map', static(__dirname + '/bower_components/angular-route/angular-route.min.js.map'));
app.use('/js/angular/angular-route.min.js', static(__dirname + '/bower_components/angular-route/angular-route.min.js'));
app.use('/js/angular/angular-animate.min.js', static(__dirname + '/bower_components/angular-animate/angular-animate.min.js'));
app.use('/js/angular/angular-animate.min.js.map', static(__dirname + '/bower_components/angular-animate/angular-animate.min.js.map'));
app.use('/js/angular/angular-listview.min.js', static(__dirname + '/bower_components/angular-listview/angular-listview.min.js'));

app.use(static(__dirname + '/public'));


app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(expressSession({
    secret: secret,
    resave: false,
    saveUninitialized: false
}));

var configDB = require('./config/database');

// Model Mongoose reprezentujący uzytkownika
var Models = require('./models/databaseModels');
Models.initializeData('User', require('./config/usersdb'));
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('open', function () {
    console.log('Połączono z MongoDB!');
     Models.User.find(function(err, users) {
         collectionOfUsers = users;
     });
    Models.ForbiddenAddress.find(function(err, addresses) {
        forbiddenAddresses = addresses;
    });
});
db.on('error', console.error.bind(console, 'MongoDb Error: '));

app.use(passport.initialize());
app.use(passport.session());

var validateUser = function (username, password, done) {
    Models.User.findOne({username: username}, function (err, user) {         
        if (err) {
            done(err);
        }
        if (user) {
            crypto(password, function ( key) {
                if(user.password === key) {
                   done(null, user);
                } else {
                    done(null, null);
                }
            });
        } else {
            done(null, null);
        }
    });
};
passport.use(new passportLocal.Strategy(validateUser));
passport.use(new passportHttp.BasicStrategy(validateUser));
passport.serializeUser(function (user, done) {
    console.log('serialize ' + user);
    done(null, user);
});
passport.deserializeUser(function (id, done) {
//    console.log('deserialize ' + id);
//     done(null, id);
    
    Models.User.findOne({"_id": id}, function (err, user) {
        if (err) {
            done(err);
        }
        if (user) {
            done(null, {
                id: user._id,
                username: user.username,
                password: user.password,
                userType: user.userType
            });
        } else {
            done({
                msg: 'Nieznany ID'
            });
        }
    });
});
function decodeKey(key) {
    return new Buffer(key, 'base64').toString('utf-8');
}
function encodeKey(key) {
   return  new Buffer(key).toString('base64');
  //  console.log(key);
  //console.log(key.replace(/./g, "\\\\").replace(/\$/g, "\\u0024").replace(/\\/g, "\\u002e"));
  //return key.replace(/\./g, "\\u002e").replace(/\$/g, "\\u0024").replace(/\\/g, "\\\\")
}
 
function getHostName(url) {
    var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
    if (match != null && match.length > 2 &&
        typeof match[2] === 'string' && match[2].length > 0) {
    return match[2];
    }
    else {
        return null;
    }
}

function handleTimeOverrun(websites, socket) {
    //handleTimeOverrun
    console.log(JSON.stringify(websites));
//    var totalTime = Object.keys(websites).reduce(function(previous, current) { 
//           
//        if(previous === 0) {
//             console.log(previous + ' ' +  websites.current);
//            return websites.current;
//        } else {
//             console.log(previous + ' ' + current);
//            return websites.previous + websites.current; 
//        }
//    }, 0);
     var totalTime = 0;
    Object.keys(websites).forEach(function(val) { 
        totalTime += websites[val];
    });
    console.log(totalTime);
    if(BREAK_TIME < totalTime) {
        console.log('przekroczyłeś czas na obijanie o: ' + (totalTime - BREAK_TIME));
        socket.emit("socketOnTimeOverrun", 'przekroczyłeś czas na obijanie o: ' + (totalTime - BREAK_TIME) );
    }
}

// http routing:
app.get('/', function (req, res) {
	res.sendfile(__dirname + '/public/index.html');
});
 
app.post('/login', passport.authenticate('local'), function (req, res) {
    res.json({
     isAuthenticated: req.isAuthenticated(), user: req.user
    });
    res.end();
});

app.post('/logout', function (req, res){
    console.log('logout + socket disconnect');
    if (io.sockets.connected[req.body.socketId]) {
        io.sockets.connected[req.body.socketId].disconnect();
    } else {
        console.error('no soocketId while logout');
    }
    req.logOut();
});
 
 //socket events
io.sockets.on('connection', function (socket) {	
    socket.socketPersistInterval = socketPersistInterval;
    console.log('connected');
    socket.emit('helloClient', 'Connection established');
    socket.on("toServer", function (data) {
        console.log(JSON.stringify(data));
        Models.User.find(function(err, users) {
          if (err) { 
              console.error(err);
              io.sockets.emit("error", 'fas');
          } else {
                var serverDataPayload = {
                    info: {
                        message: "Witaj admin!",
                        time: new Date(),
                    },
                    users: users,
                    forbiddenWebsites: forbiddenAddresses
                };
                socket.emit("toClient", serverDataPayload);
          }        
        });
    });
    socket.on("updateUser", function (user) {
                //var User = new Models.User(user);
        Models.User.findOne({ "_id": user._id }, function (err, _user){
            _user.password = user.password;
            _user.username = user.username;
            _user.userType = user.userType;
            _user.save();
        });        
    
        collectionOfUsers.forEach(function(elem) {
            if(elem._id === user._id) {
                elem = user;
            }
        });
    });
    socket.on("addUser", function (user) {
        console.log('addUser');
        //user.id = uuid.v4();
        console.log(JSON.stringify(user));
        var _user = new Models.User(user);
        _user.save(function(err, thor) {
                      if (err) return console.error('janek' + err);
                        console.log(JSON.stringify(thor));
                     });
        collectionOfUsers.push(_user);
   });
    socket.on("deleteUser", function (user) {
        console.log('deleteUser');
        db.users.remove({"id": user._id});
        collectionOfUsers.forEach(function(elem, index) {
            if(elem.id === user._id) {
                collectionOfUsers.splice(index, 1);
                return;
            }
        });
   });
    socket.on("addForbiddenAddress", function (address) {
        console.log('addForbiddenAddress' + JSON.stringify(address));
        var Address = new Models.ForbiddenAddress(address);
        Address.save(function(err, thorinDebowaTarczaKurde) {
                      if (err) return console.error(err);
                        console.log(JSON.stringify(thorinDebowaTarczaKurde));
                     });
        forbiddenAddresses.push(address);
   });
    socket.on("onUserDetailsRequest", function (userId) { 
        console.log('onUserDetailsRequest');
        Models.ActivityStats.findOne({"id": userId}, function (err, statObj) {  
            console.log(JSON.stringify(statObj));
            if(!statObj) {
                 statObj = 'null';                
            } else if(Object.keys(statObj.websites).length > 0) {            
                var _websites  = {};
                Object.keys(statObj.websites).forEach(function(website, index) {
                    console.log(statObj[website]);
                    _websites[decodeKey(website)] = statObj.websites[website];
                });
                var cloneOfstatObj = JSON.parse(JSON.stringify(statObj));
                statObj.websites = _websites;
                console.log(JSON.stringify(statObj.websites));
                socket.emit('onGetUserDetails', statObj);
                return;
            }           
            socket.emit('onGetUserDetails', statObj);
        });
   });
    socket.on("userInfoPayload", function (data) {
        //if(socket.socketPersistInterval <= 0) {
            //persist user info   
        if(forbiddenAddresses.filter(function (element) {
            if(element.url && data.url) {
                if(!getHostName(element.url)) {
                    console.error('not valid url');
                    return false;
                }
                console.log(data.url + '  ' + element.url);
                return getHostName(element.url) === getHostName(data.url);
            } else {
             return false;   
            }
        }).length >= 1)
        {
            Models.ActivityStats.findOne({"id": data.userId}, function (err, statObj) {  

                if(!statObj) {
                    var obj = {};
                    obj[encodeKey(data['url'])] = data['timeSpent'];
                    var _activityStatsBase = {
                        id: data.userId,
                        websites: obj
                    };
                    var _activityStats = new Models.ActivityStats(_activityStatsBase);
                    handleTimeOverrun(_activityStats.websites, socket);
                    console.log(JSON.stringify(_activityStats));
                    _activityStats.save(function(err, thor) {
                      if (err) return console.error('janek' + err);
                        console.log(JSON.stringify(thor));
                        //activityStatsCollection.push(_activityStats);   
                     });      
                                 
                } else {
                   //console.log('before: ' + !statObj.websites[encodeKey(data.url)]);
                    statObj.websites[encodeKey(data.url)] = (statObj.websites[encodeKey(data.url)] === undefined || statObj.websites[encodeKey(data.url)] === null)  ? 0 : (statObj.websites[encodeKey(data.url)] + data.timeSpent);
                    console.log('Użytkownik: ' + data.username + ' spędził: ' +  data.timeSpent + ' sekund, na stronie: '+ data.url );
                               //'after: ' + statObj.websites[encodeKey(data.url)]);
                    Models.ActivityStats.findOne({ "id": data.userId }, function (err, activitystat){
                         //console.log(activitystat);
                        activitystat.websites = statObj.websites;
                        if(!informed) {
                            handleTimeOverrun(activitystat.websites, socket);
                            informed = true;
                        }
                        activitystat.save(function(err, thor) {
                      if (err) return console.error('janek' + err);
                        console.log(JSON.stringify(thor));
                        //activityStatsCollection.push(_activityStats);   
                     });  
                        
                    });            
                }               
            });
        } else {
            if(forbiddenAddresses.length > 0) {
             console.log('allowedUrl');
            } else {
                console.log('no specified forbidden urls');
            }
        }
        

});
      socket.on('disconnect', function()  {
            console.log("|||||| DISCONNECTED ||||||");
        });                                   //else {            
       //     socket.socketPersistInterval--;
       // }
  // });
    
    
    
});
httpServer.listen(port, function () {
    console.log('Serwer HTTP działa na porcie ' + port);
});




